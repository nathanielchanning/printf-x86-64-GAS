.PHONY: clean

printf-gas-test: printf-gas-test.S printf-gas.o integer-to-base.o get-len.o
	as printf-gas-test.S -o printf-gas-test.o
	ld printf-gas-test.o -o printf-gas-test printf-gas.o integer-to-base.o get-len.o

printf-gas.o: printf-gas.S
	as printf-gas.S -o printf-gas.o

integer-to-base.o: integer-to-base.S
	as integer-to-base.S -o integer-to-base.o

get-len.o: get-len.S
	as get-len.S -o get-len.o

clean:
	rm *.o printf-gas-test
